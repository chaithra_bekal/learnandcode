using System.Net.Sockets;
using NUnit.Framework;

namespace ClientOperations.Tests
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }
        [TestCase("hello","general","publisher")]
        [TestCase("hey","sports","publisher")]
        [Test]
        public void CreateMessageInTheFormatPublisher_ValidFormat_ReturnsValidMessage(string message, string topicName, string userType)
        {
            Message messageObject = Publisher.CreateMessageInTheFormat(message,topicName,userType);
            Assert.AreEqual(messageObject.userType, userType);
            Assert.AreEqual(messageObject.topicName,topicName);
        }
        [TestCase("general","subscriber")]
        [TestCase("sports","subscriber")]
        [Test]
        public void CreateMessageInTheFormatSubscriber_ValidFormat_ReturnsValidMessage(string topicName, string userType)
        {
            Message messageObject = Subscriber.CreateMessageInTheFormat(topicName,userType);
            Assert.AreEqual(messageObject.userType, userType);
            Assert.AreEqual(messageObject.topicName,topicName);
        }
        [TestCase("Hi","general","publisher")]
        [Test]
        public void SerializeToJsonString_ValidFormat_ReturnsJsonString(string message, string topicName, string userType)
        {
            Message messageObject = Publisher.CreateMessageInTheFormat(message,topicName,userType);
            Protocol completeFormatObject = new Protocol();
            completeFormatObject.data = messageObject;
            Parser parser = new Parser();
            string jsonString = parser.SerializeToJsonString(completeFormatObject);
            Assert.That(jsonString.Contains(message));
        }
        [TestCase("{\"data\":{\"createdDate\":\"2/21/2021 10:46:56 PM\",\"expiryDate\":\"2/23/2021 10:46:56 PM\",\"messageString\":\"hi\",\"topicName\":\"general\",\"userType\":\"publisher\"}}","hi")]
        [Test]
        public void DeserializeToJsonObject_ValidFormat_ReturnsJsonObject(string jsonString, string messageString)
        {
            Parser parser = new Parser();
            Protocol messageObject = parser.DeserializeToJsonObject(jsonString);
            Assert.AreEqual(messageObject.data.messageString, messageString);
        }
        [TestCase("hello","BKybRVsAhXYVeEIYaK95vg==")]
        [Test]
        public void EncryptionOfMessage_ValidEncryptedMessage_ReturnsEncryptedString(string plainText, string expectedEncryptedText)
        {
            string actualEncryptedText = Encryption.EncryptStringAES(plainText);
            Assert.AreEqual(actualEncryptedText,expectedEncryptedText);
        }
        [TestCase("BKybRVsAhXYVeEIYaK95vg==","hello")]
        [Test]
        public void DecryptionOfMessage_ValidDecryptedMessage_ReturnsDecryptedString(string EncryptedText, string expectedPlainText)
        {
            string actualPlainText = Decryption.DecryptStringAES(EncryptedText);
            Assert.AreEqual(actualPlainText,expectedPlainText);
        }
    }
}