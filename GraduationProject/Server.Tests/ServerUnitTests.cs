using Microsoft.Data.SqlClient;
using NUnit.Framework;

namespace Server.Tests
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [TestCase("{\"data\":{\"createdDate\":\"2/21/2021 10:46:56 PM\",\"expiryDate\":\"2/23/2021 10:46:56 PM\",\"messageString\":\"hi\",\"topicName\":\"general\",\"userType\":\"publisher\"}}","hi")]
        [Test]
        public void ValidateMessageStringDeserializeToJsonObject_ValidFormat_ReturnsJsonObject(string jsonString, string messageString)
        {
            Parser parser = new Parser();
            Protocol messageObject = parser.DeserializeToJsonObject(jsonString);
            Assert.AreEqual(messageObject.data.messageString, messageString);
        }
        [Test]
        public void ConnectingToDatabase_ConnectionOpen_ReturnsOpenSqlConnection()
        {
            SqlConnection connection = DataBaseConnection.ConnectToDatabase();
            Assert.That(connection.State.ToString().Equals("Open"));
        }
        [Test]
        public void DisconnectingToDatabase_ConnectionClose_ReturnsCloseSqlConnection()
        {
            SqlConnection connection = DataBaseConnection.ConnectToDatabase();
            bool isConnectionStateClose = DataBaseConnection.CloseConnectionToDatabase(connection);
            Assert.That(isConnectionStateClose.Equals(true));
        }
        [TestCase("hello","BKybRVsAhXYVeEIYaK95vg==")]
        [Test]
        public void EncryptionOfMessage_ValidEncryptedMessage_ReturnsEncryptedString(string plainText, string expectedEncryptedText)
        {
            string actualEncryptedText = Encryption.EncryptStringAES(plainText);
            Assert.AreEqual(actualEncryptedText,expectedEncryptedText);
        }
        [TestCase("BKybRVsAhXYVeEIYaK95vg==","hello")]
        [Test]
        public void DecryptionOfMessage_ValidDecryptedMessage_ReturnsDecryptedString(string EncryptedText, string expectedPlainText)
        {
            string actualPlainText = Decryption.DecryptStringAES(EncryptedText);
            Assert.AreEqual(actualPlainText,expectedPlainText);
        }
    }
}