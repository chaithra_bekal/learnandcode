﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using Microsoft.Data.SqlClient;

public class Server
{
    public static ManualResetEvent allDone = new ManualResetEvent(false);
    public static void StartListening()
    {
        ServerConstants serverConstants = new ServerConstants();
        IPHostEntry ipHostInfo = Dns.GetHostEntry(serverConstants.host);
        IPAddress ipAddress = ipHostInfo.AddressList[0];
        IPEndPoint localEndPoint = new IPEndPoint(ipAddress, serverConstants.portNumber);

        Socket listener = new Socket(ipAddress.AddressFamily,
            SocketType.Stream, ProtocolType.Tcp);

        try
        {
            listener.Bind(localEndPoint);
            listener.Listen(100);

            while (true)
            {
                allDone.Reset();

                Console.WriteLine("Waiting for a connection...");
                listener.BeginAccept(
                    new AsyncCallback(ConnectToClientSocket),
                    listener);

                allDone.WaitOne();
            }

        }
        catch (SocketException)
        {
            Console.WriteLine("Not able to connect to server... try again!");
        }

        Console.WriteLine("\nPress ENTER to continue...");
        Console.Read();

    }

    public static void ConnectToClientSocket(IAsyncResult asyncResult)
    {
        
        allDone.Set();
        Socket listener = (Socket)asyncResult.AsyncState;
        Socket handler = listener.EndAccept(asyncResult);
        
        Console.WriteLine("Client socket port: " + handler.RemoteEndPoint.ToString());
        
        StateObject state = new StateObject();
        state.workSocket = handler;
    
        handler.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
            new AsyncCallback(PerformQueueOperationsSendDataToServer), state);
        
    }

    public static void PerformQueueOperationsSendDataToServer(IAsyncResult asyncResult) //callback method
    {
        Parser parserObject = new Parser();
        String clientMessageString = String.Empty;
        StateObject state = (StateObject)asyncResult.AsyncState;
        Socket handler = state.workSocket;
        
        int bytesRead = handler.EndReceive(asyncResult);
        SqlConnection connection = DataBaseConnection.ConnectToDatabase();
       

        if (bytesRead > 0)
        {
            clientMessageString = UtilityFunctions.GetClientMessageString(state, clientMessageString, bytesRead);
            
            if (clientMessageString.IndexOf("") > -1)
            {
                Protocol jsonData = UtilityFunctions.PushOrPullMessages(clientMessageString, connection, parserObject,handler);
                SendToClient(handler, jsonData);
            }
            else
            {
                handler.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                new AsyncCallback(PerformQueueOperationsSendDataToServer), state);
            }
        }
        bool isClose = DataBaseConnection.CloseConnectionToDatabase(connection);
        
    }
    private static void SendToClient(Socket handler, Protocol parsedData)
    {
        Parser parserObject = new Parser();
        string parsedString = parserObject.SerializeToJsonString(parsedData);
        byte[] byteData = Encoding.ASCII.GetBytes(parsedString);
        
        handler.BeginSend(byteData, 0, byteData.Length, 0,
            new AsyncCallback(EndSendingDataToClient), handler);
    }

    private static void EndSendingDataToClient(IAsyncResult asyncResult) //callback method
    {
        try
        {
            Socket handler = (Socket)asyncResult.AsyncState;
            int bytesSent = handler.EndSend(asyncResult);
            Console.WriteLine("Sent {0} bytes to client.", bytesSent);
            handler.Shutdown(SocketShutdown.Both);
            handler.Close();
            
        }
        catch(SocketException socketException)
        {
            Console.WriteLine(socketException.ToString());
        }
        catch (Exception e)
        {
            Console.WriteLine(e.ToString());
        }
    }

    public static int Main(String[] args)
    {
        StartListening();
        return 0;
    }
}