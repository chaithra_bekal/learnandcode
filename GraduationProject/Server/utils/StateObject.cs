using System.Net.Sockets;
using System.Text;
public class StateObject
{
    public const int BufferSize = 1024;

    public byte[] buffer = new byte[BufferSize];

    public StringBuilder stringBuilder = new StringBuilder();

    public Socket workSocket = null;
}