using System;
using System.Collections;
using System.Net.Sockets;
using System.Text;
using Microsoft.Data.SqlClient;

public class UtilityFunctions
{
    public static string GetClientMessageString(StateObject state, string clientMessageString, int bytesRead)
    {
        state.stringBuilder.Append(Encoding.ASCII.GetString(
                state.buffer, 0, bytesRead));
        clientMessageString = state.stringBuilder.ToString();
        return clientMessageString;
    }
    public static Protocol PushOrPullMessages(string clientMessageString, SqlConnection connection, Parser parserObject, Socket handler)
    {
        Protocol jsonData = parserObject.DeserializeToJsonObject(clientMessageString);

        DeadLetterFunctionality.CheckExpiredMessagesAndPushToDeadLetterQueue(connection);

        if (jsonData.data.userType == "publisher")
        {
            jsonData.data.messageString = Decryption.DecryptStringAES(jsonData.data.messageString);
            WritingToDatabase.FetchQueryAndExecute(handler, jsonData, connection);
            Console.WriteLine("Read {0} bytes from socket. \n Data : {1}",
            clientMessageString.Length, jsonData.data.messageString);
            jsonData.data.messageString = Encryption.EncryptStringAES(jsonData.data.messageString);
        }
        else if (jsonData.data.userType == "subscriber")
        {
            string messagesFromQueue = null;
            Queue retrievedQueue = ReadingFromDatabase.RetrieveMessagesFromDatabase(handler, jsonData, connection);
            foreach (String messageInQueue in retrievedQueue)
            {
                Console.WriteLine(messageInQueue);
                messagesFromQueue = messagesFromQueue + messageInQueue + "\n";
            }
            jsonData.data.messageString = messagesFromQueue;
        }
        return jsonData;
    }
}