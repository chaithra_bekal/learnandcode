using System;
using System.Collections;
using Microsoft.Data.SqlClient;

public class DeadLetterFunctionality {
    public static void CheckExpiredMessagesAndPushToDeadLetterQueue(SqlConnection connection)
    {
        FetchSelectQueryAndExecute("General", connection, 1);

        FetchSelectQueryAndExecute("Politics", connection, 2);

        FetchSelectQueryAndExecute("Sports", connection, 3);
    }
    public static void  FetchSelectQueryAndExecute(string queueName, SqlConnection connection, int queueId)
    {
        DatabaseQueries fetchQuery = new DatabaseQueries();
        SqlDataAdapter adapter = new SqlDataAdapter();
        SqlCommand command = new SqlCommand();
        string sqlSelectExpiredMessages = fetchQuery.ReturnSqlSelectExpiredMessages(queueName);
        Queue deadMessages = ExecuteSelectQueryAndReturnDeadMessages(fetchQuery, command, connection, queueId, queueName);
        MoveMessagesFromQueueToDatabase(deadMessages,queueId,fetchQuery,queueName,command,connection,adapter);
    }
    public static void MoveMessagesFromQueueToDatabase(Queue deadMessages, int queueId, DatabaseQueries fetchQuery, string queueName, SqlCommand command, SqlConnection connection, SqlDataAdapter adapter)
    {
        foreach (String deadMessage in deadMessages)
        {
            ExecuteInsertToDeadLetterQueueTable(fetchQuery, queueId, deadMessage, command, connection, adapter);
            ExecuteDeleteFromQueueTable(fetchQuery, queueName, deadMessage, command, connection, adapter);
        }
    }
    public static Queue ExecuteSelectQueryAndReturnDeadMessages(DatabaseQueries fetchQuery, SqlCommand command, SqlConnection connection, int queueId, string queueName)
    {
        string sqlSelectExpiredMessages = fetchQuery.ReturnSqlSelectExpiredMessages(queueName);
        command = new SqlCommand(sqlSelectExpiredMessages, connection);
        using (SqlDataReader dataReader = command.ExecuteReader())
            {
                Queue deadMessages = new Queue(); 
                while (dataReader.Read())
                {    
                    deadMessages.Enqueue(dataReader[2]);
                }
                return deadMessages;
            }
    }
    public static void ExecuteInsertToDeadLetterQueueTable(DatabaseQueries fetchQuery, int queueId, string message, SqlCommand command, SqlConnection connection, SqlDataAdapter adapter)
    {
        string sqlInsertDeadMessageQuery = fetchQuery.ReturnSqlInsertMessagesToDeadLetterQueueTableQuery(queueId,message);
        ExecuteCommand(connection, sqlInsertDeadMessageQuery, command, adapter);
        
    }
    public static void ExecuteDeleteFromQueueTable(DatabaseQueries fetchQuery, string queueName, string deadMessage, SqlCommand command, SqlConnection connection, SqlDataAdapter adapter)
    {
        string sqlDeleteFromQueueQuery = fetchQuery.ReturnSqlDeleteExpiredMessagesQuery(queueName, deadMessage);
        ExecuteCommand(connection,sqlDeleteFromQueueQuery,command,adapter);
    }
    public static void ExecuteCommand(SqlConnection connection, string sqlQuery, SqlCommand command, SqlDataAdapter adapter)
    {
        command = new SqlCommand(sqlQuery, connection);
        adapter.InsertCommand = new SqlCommand(sqlQuery,connection);
        adapter.InsertCommand.ExecuteNonQuery();
        command.Dispose();
    }
}