using System;
public class DatabaseQueries{
    public static int generalQueueId = 1;
    public static int politicsQueueId = 2;
    public static int sportsQueueId = 3;
    public string ReturnSqlInsertDataToRootTableQuery(string clientPortNumber)
    {
        string sqlInsertDataQuery = "insert into IMQRootTable(client_name) values('client" + clientPortNumber + "')";
        return sqlInsertDataQuery;
    }
    
    public string ReturnSqlInsertDataToClientTableQuery(string content, string clientPortNumber, string client_timeStamp)
    {
        string returnSqlInsertDataToClientTableQuery = "insert into ClientTable(client_timeStamp,client_message,client_name) values('" + client_timeStamp + "','" + content + "','client" + clientPortNumber +"')";
        return returnSqlInsertDataToClientTableQuery;
    }
    public string ReturnSqlInsertDataToParticularTopicTableQuery(string content, string client_timestamp_created, string client_timestamp_expiry, string topicName)
    {
        string returnSqlInsertDataToGeneralTopicTableQuery = "insert into " + topicName + "(queue_id,message_from_publisher,created_date,expiring_date) values('1','" + content +"','" + client_timestamp_created + "','" + client_timestamp_expiry + "')";
        return returnSqlInsertDataToGeneralTopicTableQuery;
    }
    public string ReturnSqlSelectDataFromParticularQueueTableQuery(string topicName)
    {
        string returnSqlSelectDataFromGeneralQueueTableQuery = "select * from " + topicName;
        return returnSqlSelectDataFromGeneralQueueTableQuery;
    }
    public string ReturnSqlSelectExpiredMessages(string queueName)
    {
        string returnSqlSelectExpiredMessagesQuery = "select * from "+ queueName +" where expiring_date < '"+ DateTime.Now.ToString() +"'";
        return returnSqlSelectExpiredMessagesQuery;
    }
    public string ReturnSqlInsertMessagesToDeadLetterQueueTableQuery(int queueId, string message)
    {
        string returnSqlInsertMessagesToDeadLetterQueueTableQuery = "insert into DeadLetterQueue(queue_id,message) values('" + queueId + "','" + message + "')";
        return returnSqlInsertMessagesToDeadLetterQueueTableQuery;
    }
    public string ReturnSqlDeleteExpiredMessagesQuery(string queueName, string deadMessage)
    {
        string returnSqlDeleteExpiredMessagesQuery = "delete from " + queueName + " where message_from_publisher = '" + deadMessage +"'";
        return returnSqlDeleteExpiredMessagesQuery;
    }
}