using Microsoft.Data.SqlClient;

public class DataBaseConnection
{

    public static SqlConnection ConnectToDatabase()
    {
        ServerConstants serverConstants = new ServerConstants();
        string connectionString = @"Data Source=" + serverConstants.host +";Initial Catalog=" + serverConstants.databaseName + ";Integrated Security=True;";
        SqlConnection connection;
        connection = new SqlConnection(connectionString); 
        connection.Open();
        return connection;
    }

    public static bool CloseConnectionToDatabase(SqlConnection connection)
    {
        connection.Close();
        if(connection.State.ToString() == "Closed")
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
}