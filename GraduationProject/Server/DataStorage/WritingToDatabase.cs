using Microsoft.Data.SqlClient;
using System.Net.Sockets;
public class WritingToDatabase {
    public static void FetchQueryAndExecute(Socket handler, Protocol jsonData, SqlConnection connection)
    {
        DatabaseQueries fetchQuery = new DatabaseQueries();
        SqlCommand command = new SqlCommand(); 

        string clientPortNumber = handler.RemoteEndPoint.ToString().Substring(6);
        SqlDataAdapter adapter = new SqlDataAdapter();

        string sqlInsertData = fetchQuery.ReturnSqlInsertDataToRootTableQuery(clientPortNumber);
        ExecuteCommand(connection,sqlInsertData,command,adapter);

        string sqlInsertDataToRootTable = fetchQuery.ReturnSqlInsertDataToClientTableQuery(jsonData.data.messageString, clientPortNumber, jsonData.data.createdDate);
        ExecuteCommand(connection,sqlInsertDataToRootTable,command,adapter);

        string sqlInsertDataToGeneralTopicTable = fetchQuery.ReturnSqlInsertDataToParticularTopicTableQuery(jsonData.data.messageString, jsonData.data.createdDate, jsonData.data.expiryDate, jsonData.data.topicName);
        ExecuteCommand(connection,sqlInsertDataToGeneralTopicTable,command,adapter);
    }
    public static void ExecuteCommand(SqlConnection connection, string sqlQuery, SqlCommand command, SqlDataAdapter adapter)
    {
        command = new SqlCommand(sqlQuery,connection);
        adapter.InsertCommand = new SqlCommand(sqlQuery,connection);
        adapter.InsertCommand.ExecuteNonQuery();
        command.Dispose();
    }

}