using System;
using System.IO;
using System.Net.Sockets;
using System.Text;

public class StoreDataInFile
{

public static void WriteToFile(Socket handler, string content)
    {
        ServerConstants serverConstants = new ServerConstants();
        string portNumber = handler.RemoteEndPoint.ToString().Substring(6);
        string fileName = serverConstants.filePath + portNumber + ".txt";
        if (!File.Exists(fileName))
        {
            using (FileStream fs = File.Create(fileName))
            {
                Byte[] data = new UTF8Encoding(true).GetBytes(content);
                fs.Write(data, 0, data.Length);
            }
        }
        else
        {
            using (FileStream fs = File.OpenWrite(fileName))
            {
                Byte[] data = new UTF8Encoding(true).GetBytes(content);
                fs.Write(data, 0, data.Length);
            }
        }

    }
}