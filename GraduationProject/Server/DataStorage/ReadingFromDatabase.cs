using Microsoft.Data.SqlClient;
using System.Net.Sockets;
using System.Collections;

public class ReadingFromDatabase {

    public static Queue RetrieveMessagesFromDatabase(Socket handler, Protocol jsonData, SqlConnection connection)
    {
        DatabaseQueries fetchQuery = new DatabaseQueries();
        SqlCommand command = new SqlCommand();
        SqlDataAdapter adapter = new SqlDataAdapter();
        string sqlSelectDataFromParticularTableQueueTableQuery = fetchQuery.ReturnSqlSelectDataFromParticularQueueTableQuery(jsonData.data.topicName);
        Queue messagesQueue = ExecuteSelectQueries(sqlSelectDataFromParticularTableQueueTableQuery, command, connection);
        return messagesQueue;
    }
    public static Queue ExecuteSelectQueries(string sqlSelectCommand, SqlCommand command, SqlConnection connection)
    {
        command = new SqlCommand(sqlSelectCommand, connection);
        using (SqlDataReader dataReader = command.ExecuteReader())
            {
                Queue q = new Queue(); 
                while (dataReader.Read())
                {    
                    q.Enqueue(dataReader[2]);
                }
                return q;
            }
    }
}