using System.Net.Sockets;
using System;
namespace ClientOperations
{
public class Subscriber : Client {
    public static void PullMessagesFromQueue(Socket client, string topicName, string user)
    {
        ClientConstants clientConstants = new ClientConstants();
        Parser parserObject = new Parser();

        Message messageInTheFormat = CreateMessageInTheFormat(topicName, user);

        SendToServer(client, messageInTheFormat);
        sendDone.WaitOne();

        ReceiveFromClient(client);
        receiveDone.WaitOne();

        Protocol responseParsedData = parserObject.DeserializeToJsonObject(response);
        Console.WriteLine("Messages in queue are : \n{0}", responseParsedData.data.messageString);

        client.Shutdown(SocketShutdown.Both);
    }

    public static Message CreateMessageInTheFormat(string topicName, string userType)
    {
        Message messageObject = new Message();

        messageObject.topicName = topicName;
        messageObject.userType = userType;

        return messageObject;
    }
}
}