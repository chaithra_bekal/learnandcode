public class Protocol {
    public double version = 1.0;
    public Message data { get; set; }
    public string dataFormat = "json";
    public Request requestType;
    public Response messageStatus;

}