using System.Text.Json;

public class Parser {
    public string SerializeToJsonString(Protocol messageObject) 
    {
        string jsonString = JsonSerializer.Serialize(messageObject);
        return jsonString;
    }
    public Protocol DeserializeToJsonObject(string jsonString)
    {
        Protocol jsonData = JsonSerializer.Deserialize<Protocol>(jsonString);
        return jsonData;
    }
}