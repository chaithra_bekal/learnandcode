using System;
using System.Net.Sockets;
namespace ClientOperations
{
public class Publisher : Client
{

    public static void PushMessageToQueue(Socket client, string topicName, string user)
    {
        ClientConstants clientConstants = new ClientConstants();
        Parser parserObject = new Parser();
        
        Console.WriteLine("Enter message: \n");
        string message = Console.ReadLine();
        string encryptedMessage = Encryption.EncryptStringAES(message);

        Message messageInTheFormat = CreateMessageInTheFormat(encryptedMessage, topicName, user);

        SendToServer(client, messageInTheFormat);
        sendDone.WaitOne();

        ReceiveFromClient(client);
        receiveDone.WaitOne();

        Protocol responseParsedData = parserObject.DeserializeToJsonObject(response);
        responseParsedData.data.messageString = Decryption.DecryptStringAES(responseParsedData.data.messageString);
        Console.WriteLine("Message pushed : {0}", responseParsedData.data.messageString);

        client.Shutdown(SocketShutdown.Both);
    }
    public static Message CreateMessageInTheFormat(string encryptedMessage, string topicName, string userType)
    {
        Message messageObject = new Message();

        messageObject.createdDate = DateTime.Now.ToString();
        messageObject.expiryDate = DateTime.Now.AddDays(2.0).ToString();
        messageObject.messageString = encryptedMessage;
        messageObject.topicName = topicName;
        messageObject.userType = userType;

        return messageObject;
    }

}
}