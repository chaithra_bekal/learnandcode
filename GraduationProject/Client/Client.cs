﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Text;
namespace ClientOperations
{
public class Client
{
    public static ManualResetEvent connectDone = new ManualResetEvent(false);
    public static ManualResetEvent sendDone = new ManualResetEvent(false);
    public static ManualResetEvent receiveDone = new ManualResetEvent(false);
    public static String response = String.Empty;
    public static void StartClient()
    {
        ClientConstants clientConstants = new ClientConstants();
        Parser parserObject = new Parser();
        try
        {
            IPHostEntry ipHostInfo = Dns.GetHostEntry(clientConstants.hostName);
            IPAddress ipAddress = ipHostInfo.AddressList[0];
            IPEndPoint remoteEndpoint = new IPEndPoint(ipAddress, clientConstants.port);

            Socket client = new Socket(ipAddress.AddressFamily,
                SocketType.Stream, ProtocolType.Tcp);

            client.BeginConnect(remoteEndpoint,
                new AsyncCallback(ConnectToServer), client);
            connectDone.WaitOne();

            Console.WriteLine("Enter Publisher or subscriber: ");
            clientConstants.user = Console.ReadLine().ToLower();
            while(clientConstants.topicName == null)
            {
            Console.WriteLine("The available topics are \n1. General\n2. Politics\n3. sports\n");

            Console.WriteLine("Enter topic name: ");

            string inputTopicName = Console.ReadLine().ToLower();
            int totalNumberOfTopicsAvailable = TopicService.FindNumberOfTopics();
            for(int i=0; i<totalNumberOfTopicsAvailable; i++)
            {
                if(inputTopicName == TopicService.topics[i])
                {
                    clientConstants.topicName = inputTopicName;
                }
            }
            if(clientConstants.topicName == null)
            {
                Console.WriteLine("Topic doesn't exist, re-enter from the list");
            }
            }
            if(clientConstants.user == "publisher")
            {
                Publisher.PushMessageToQueue(client, clientConstants.topicName, clientConstants.user);
            }
            else if(clientConstants.user == "subscriber")
            {
                Subscriber.PullMessagesFromQueue(client, clientConstants.topicName, clientConstants.user);
            }
            client.Close();
        }
        catch (Exception e)
        {
            Console.WriteLine(e.ToString());
        }
    }
    private static void ConnectToServer(IAsyncResult asyncResult)
    {
        try
        {
            Socket client = (Socket)asyncResult.AsyncState;

            client.EndConnect(asyncResult);

            Console.WriteLine("Socket connected to {0}",
                client.RemoteEndPoint.ToString());

            connectDone.Set();
        }
        catch (Exception e)
        {
            Console.WriteLine(e.ToString());
        }
    }

    public static void ReceiveFromClient(Socket client)
    {
        try
        {
            StateObject state = new StateObject();
            state.workSocket = client;

            client.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                new AsyncCallback(EndReceivingDataFromServer), state);
        }
        catch (Exception e)
        {
            Console.WriteLine(e.ToString());
        }
    }

    private static void EndReceivingDataFromServer(IAsyncResult asyncResult) //callback method
    {
        try
        {
            StateObject state = (StateObject)asyncResult.AsyncState;
            Socket client = state.workSocket;

            int bytesRead = client.EndReceive(asyncResult);

            if (bytesRead > 0)
            {
                state.stringBuilder.Append(Encoding.ASCII.GetString(state.buffer, 0, bytesRead));

                client.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                    new AsyncCallback(EndReceivingDataFromServer), state);
            }
            else
            {
                if (state.stringBuilder.Length > 1)
                {
                    response = state.stringBuilder.ToString();
                }
                receiveDone.Set();
            }
        }
        catch (Exception e)
        {
            Console.WriteLine(e.ToString());
        }
    }

    public static void SendToServer(Socket client, Message messageObject)
    {
        Parser parserObject = new Parser();
        Response responseStatus = new Response();
        responseStatus.messageStatus = "Success";
        Protocol completeFormatObject = new Protocol();
        completeFormatObject.data = messageObject;
        completeFormatObject.messageStatus = responseStatus;
        string jsonString = parserObject.SerializeToJsonString(completeFormatObject);
        Console.WriteLine("The json string is: " + jsonString);

        byte[] byteData = Encoding.ASCII.GetBytes(jsonString);

        client.BeginSend(byteData, 0, byteData.Length, 0,
            new AsyncCallback(EndSendingDataToServer), client);
    }

    private static void EndSendingDataToServer(IAsyncResult asyncResult) //callback method
    {
        try
        {
            Socket client = (Socket)asyncResult.AsyncState;

            int bytesSent = client.EndSend(asyncResult);
            Console.WriteLine("Sent {0} bytes to server.", bytesSent);

            sendDone.Set();
        }
        catch (Exception e)
        {
            Console.WriteLine(e.ToString());
        }
    }

    public static int Main(String[] args)
    {
        StartClient();
        return 0;
    }
}
}
